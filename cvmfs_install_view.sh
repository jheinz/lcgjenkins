#!/bin/bash
weekday=`date +%a`
sudo -i -u cvsft<<EOF
shopt -s nocasematch
for iterations in {1..10}
do
  if [[ "${BUILDMODE}" == "nightly" ]]; then
      cvmfs_server transaction sft-nightlies.cern.ch
  else
      cvmfs_server transaction sft.cern.ch
  fi
  if [ "\$?" == "1" ]; then
    if  [[ "\$iterations" == "10" ]]; then
      echo "Too many tries... "

      # After 10 tries in nightly mode the opened transaction is forcibly aborted
      # Only during the night ( between 7pm and 8am)
      H=`date +%H`
      if [[ "${BUILDMODE}" == "nightly" && (19 -le "$H" || "$H" -lt 8) ]]; then
          echo "Forcing transaction abortion"
          cvmfs_server abort -f sft-nightlies.cern.ch
          cvmfs_server transaction sft-nightlies.cern.ch
      else
          exit 1
      fi

    else
       echo "Transaction is already open. Going to sleep..."
       sleep 10m
    fi
  else
    break
  fi
done

echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
echo VIEW INSTALLATION
echo LCG_${LCG_VERSION}_$PLATFORM.txt
echo "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"

export COMPILER=${COMPILER}
array=(${PLATFORM//-/ })
comp=`echo ${array[2]}`

echo "THIS IS THE WEEKDAY OF TODAY: ------> $weekday"


echo "This is the value of the BUILDMODE: ---> ${BUILDMODE}"

if [[ "${BUILDMODE}" == "nightly" ]]; then
    export NIGHTLY_MODE=1
    if [ -f "/cvmfs/sft-nightlies.cern.ch/lcg/nightlies/${LCG_VERSION}/$weekday/isDone-$PLATFORM" ] ; then
      echo "The installation of the nightly is completed with all packages, let's go for the view creation"
      $WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -l /cvmfs/sft-nightlies.cern.ch/lcg/nightlies/${LCG_VERSION}/$weekday -p $PLATFORM -d -B /cvmfs/sft-nightlies.cern.ch/lcg/views/${LCG_VERSION}/$weekday/$PLATFORM
      rm /cvmfs/sft-nightlies.cern.ch/lcg/views/${LCG_VERSION}/latest/$PLATFORM
      cd /cvmfs/sft-nightlies.cern.ch/lcg/views/${LCG_VERSION}/latest
      ln -s ../$weekday/$PLATFORM
    elif [ -f "/cvmfs/sft-nightlies.cern.ch/lcg/nightlies/${LCG_VERSION}/$weekday/isDone-unstable-$PLATFORM" ]; then
        echo "The installation has not been completed and we do not create the view"
    fi

    cd $HOME
    cvmfs_server publish sft-nightlies.cern.ch

elif [[ "${BUILDMODE}" == "release" ]]; then
    $WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -l /cvmfs/sft.cern.ch/lcg/releases -p $PLATFORM -r ${LCG_VERSION} -d -B /cvmfs/sft.cern.ch/lcg/views/LCG_${LCG_VERSION}/$PLATFORM

    if [ "\$?" = "0" ]; then
      echo "The creation of the views has worked"
      cd $HOME
      cvmfs_server publish sft.cern.ch
    else
      echo "The creation of the views has not worked. We exit here"
      cd $HOME
      cvmfs_server abort -f sft.cern.ch
      exit 1
    fi
fi
EOF
