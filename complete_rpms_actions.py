#!/usr/bin/env python

import os, re
import shutil
import subprocess
import sys
import errno
import argparse
import shutil

parameters = sys.argv
version = parameters[1]

tmparea="/eos/project/l/lcg/incoming_rpms/"
officialarea="/eos/project/l/lcg/www/lcgpackages/rpms/"
lcg_subdirectory="LCG_"+version+"release"
lcg_directory=officialarea+lcg_subdirectory
os.environ['EOS_MGM_URL']="root://eosuser.cern.ch"
list_reference=[]
list_official=[]
common_rpms=[]
list_to_copy=[]
full_rmps=[]

if not os.path.exists(lcg_directory):
    os.makedirs(lcg_directory)

os.chdir(officialarea)
for j in os.listdir('.'):
    if not "LCG_" in j:
        rpm_prefix = j.split("-1.0.0")[0]
        list_official.append(rpm_prefix)

   
os.chdir(tmparea)
for f in os.listdir('.'):
    if not "LCG_" in f:
        rpm_prefix = f.split("-1.0.0")[0]
        list_reference.append(rpm_prefix)

common_rpms = set(list_reference).intersection(list_official)

for item in list_reference:
    if item not in common_rpms:
        list_to_copy.append(item)
    else:
        pass

for f in os.listdir('.'):
    if "LCG_" in f:
        if version in f:
            bashcommand="xrdcp %s root://eosuser.cern.ch/%s/%s"  %(f, officialarea, lcg_subdirectory)                                                                                                                                       
            process = subprocess.call(bashcommand, shell=True)                                                                                                                                                         

for t in list_to_copy:
    bashcommand="xrdcp %s* root://eosuser.cern.ch/%s"  %(t, officialarea)
    process = subprocess.call(bashcommand, shell=True) 

        
