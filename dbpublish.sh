#!/bin/bash

if [[ $BUILDTYPE == "Debug" ]]
  then
      CORRECTEDBUILDTYPE="dbg" 
  else
      CORRECTEDBUILDTYPE="opt"
fi
# We have to remove the binutils part from the compiler
CORRECTEDCOMPILER=$(echo ${COMPILER} | sed 's/binutils//g' | sed 's/native/gcc54/g')

export PLATFORM="x86_64-${OSVERS}-${CORRECTEDCOMPILER}-${CORRECTEDBUILDTYPE}"

cd $WORKSPACE
git clone https://:@gitlab.cern.ch:8443/sft/lcgcmake.git
cd lcgcmake
git checkout master
git pull origin master

# Publication in the lcgsoft DB

cd $WORKSPACE
jsonfile="$SLOT-$PLATFORM.json"
today=`date +%Y-%m-%d`
weekday=`date +%a` 
echo weekday=${weekday} >> tmp_properties
export PYTHONPATH=$WORKSPACE/lcgjenkins/lcgsoft/webpage:$PYTHONPATH

# Update DB from file
python lcgjenkins/lcgsoft/update_packages_from_file.py $WORKSPACE/lcgcmake/documentation/packages.json
python lcgjenkins/lcgsoft/update_releases_from_file.py $WORKSPACE/lcgcmake/documentation/releases.json


# Remove compilation if already exists
python lcgjenkins/lcgsoft/clear_release_platform.py $SLOT $PLATFORM


if [[ $NIGHTLY == "true" ]]; then
    lcgjenkins/lcgsoft/release_summary_reader.py $SLOT $PLATFORM nightly
else
    lcgjenkins/lcgsoft/release_summary_reader.py $SLOT $PLATFORM release
fi

if [[ ! -f $WORKSPACE/$jsonfile ]]; then
    exit 1
fi

lcgjenkins/lcgsoft/fill_release.py $WORKSPACE/$jsonfile -d $today -e "$DESCRIPTION" --out $WORKSPACE/lcgcmake/documentation

if [[ $NIGHTLY == "false" ]]; then
    lcgjenkins/lcgsoft/generate_changelog.py --save
fi

# Make commit if packages.json has been changed
cd $WORKSPACE/lcgcmake/documentation
git add packages.json
git add releases.json
git commit -m "[Jenkins] update packages.json and releases.json"
kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
git push origin master

cd $WORKSPACE

rm $WORKSPACE/$jsonfile 

