#!/usr/bin/env python

import argparse

from releasepy import ReleaseTree

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-p', '--platform', help="Platform to check", default='', dest='platform')
    parser.add_argument('-a', '--checkall', help="Check RPMS for all platforms", default=False, action='store_true', dest='allplatforms')
    parser.add_argument('-r', '--release-number', help="Release number", default='auto', dest='releasever')
    parser.add_argument('-e', '--endsystem', help="installation check in CVMFS or AFS", default='CVMFS', dest='endsystem')

    args = parser.parse_args()

    print "Starting RPM release check..."

    release = ReleaseTree(args.endsystem, args.releasever)

    if not args.platform and not args.allplatforms:
        raise RuntimeError("Platform to check or -a(--checkall to check all platforms) option must be specified")

    if args.allplatforms:
        release.checkRPMAllPlatforms()
    else:
        release.checkAllRPMS(args.platform)

if __name__ == "__main__":
  main()
