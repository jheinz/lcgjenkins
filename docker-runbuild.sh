#!/bin/bash -x

# Script to run full LCG builds inside docker containers
# Beware the code here highly relies on multiple variable defined inside the
# jenkins job configuration

# Docker command
# -w directory   			                 Working directory inside the container
# -e variable=value				             Environment variable to define inside the container
#    LCG_VERSION=$LCG_VERSION 				 -> Needed inside cmake/ctest commands
#    LCG_IGNORE=$LCG_IGNORE 				   -> Needed inside cmake/ctest commands
#    BUILDMODE=$BUILDMODE 					   -> Needed inside cmake/ctest commands
#    LCG_INSTALL_PREFIX=$LCG_INSTALL_PREFIX -> Needed inside cmake/ctest commands
#    LCG_EXTRA_OPTIONS=$LCG_EXTRA_OPTIONS 	-> Needed inside cmake/ctest commands
#    TARGET=$TARGET							            -> Needed inside cmake/ctest commands
#    TEST_LABELS=$TEST_LABELS 				      -> Needed by tests, it determines kind of tests to run
#    USER=sftnight			                    -> Needed by some CORAL tests which rely on this env variable
#	   SHELL=$SHELL			                      -> Needed in some package install instructions using $ENV{SHELL}
#    GIT_COMMIT=$GIT_COMMIT	                -> Needed to avoid repeating update command inside container (otherwise update status appears as failed in cdash)
#    PROPERTIES_PATH=<PATH>                 -> Path to save properties file with info for the triggered jobs
#    RUN_TEST                               -> Boolean value if the test should run after build
#    MODE                                   -> Mode in CDash - information required by test
# -u username 	 				               User to run all build instructions
# --name $NAME                         Name to identify the container
# --hostname $HOSTNAME-docker          Name to identify the docker host (used for cdash)
# --cpus=$DOCKER_CPUS			             CPU container restriction
# -v host_path:container_path 	       Folder to bind from docker host to docker container
# gitlab-registry.cern.ch/sft/docker:<image_name>   image and tag to run in the docker container
# <command>                                         command to execute inside the container (here: script with build instructions followed by params)

export WORKSPACE_HOST=$WORKSPACE
export WORKSPACE='/build/jenkins/workspace'

touch $WORKSPACE_HOST/controlfile

# Choose the correct docker image
case "$LABEL" in
lcg_docker_cc7)
    DOCKER_IMAGE=lcg-cc7
    ;;
lcg_docker_slc6)
	DOCKER_IMAGE=lcg-slc6
    ;;
lcg_docker_ubuntu16)
	DOCKER_IMAGE=lcg-ubuntu16
    ;;
lcg_docker_ubuntu18)
	DOCKER_IMAGE=lcg-ubuntu18
    ;;
docker-thin)
	DOCKER_IMAGE=cc7-thin
    ;;
*) echo "Docker image $IMAGE not configured"
   ;;
esac

# Extract name from build tag
export NAME=`echo $BUILD_TAG | tr "," "-" | tr "=" "-"`

# Prepare number of cpus to use in the container
TOTALCPU=`nproc --all`
CONTAINERS_LIMIT=$(($EXECUTOR_NUMBER+1))

if [ -z $DOCKER_CPUS ]; then
	DOCKER_CPUS=$(($TOTALCPU/$CONTAINERS_LIMIT))
fi

USER_ID=$(id $(whoami) -u)
GROUP_ID=$(id $(whoami) -g)

if [ -n "$COPY_LOGS" ]; then
    COPY_LOGS=OFF
fi

# Pull image from gitlab
docker pull gitlab-registry.cern.ch/sft/docker:$DOCKER_IMAGE

# Find out script to run inside the container
if [ $LCG_VERSION == experimental ]; then
  build_script=runexperimental-docker.sh
  MODE=Experimental
else
  build_script=runall-docker.sh
  MODE=Production
fi

#Preparation for test
if [ ! -e "$WORKSPACE_HOST/lcgtest" ]; then
    #Not required to clone this repository
    mkdir $WORKSPACE_HOST/lcgtest
fi

if [ -e $WORKSPACE_HOST/docker ]; then
    rm -rf $WORKSPACE_HOST/docker
fi
mkdir -p $WORKSPACE_HOST/docker
cat > rundocker.sh << EOF
#!/usr/bin/bash
docker run -it --rm -e WORKSPACE=$WORKSPACE \
-e LCG_VERSION=$LCG_VERSION \
-e LCG_IGNORE='$LCG_IGNORE' \
-e BUILDMODE=$BUILDMODE \
-e LCG_INSTALL_PREFIX=$LCG_INSTALL_PREFIX \
-e LCG_EXTRA_OPTIONS="$LCG_EXTRA_OPTIONS"   \
-e TARGET=$TARGET  \
-e TEST_LABELS=$TEST_LABELS \
-e USER=sftnight \
-e SHELL=$SHELL \
-e GIT_COMMIT=$GIT_COMMIT \
-e PROPERTIES_PATH=/lcgjenkins \
-e RUN_TEST=$RUN_TEST \
-e MODE=$MODE \
-e COPY_LOGS=$COPY_LOGS \
-u sftnight \
-v /ccache:/ccache \
-v /ec/conf:/ec/conf \
-v /cvmfs:/cvmfs \
--mount type=bind,source='$WORKSPACE_HOST/docker',target=/build/jenkins/workspace \
    gitlab-registry.cern.ch/sft/docker:$DOCKER_IMAGE  \
    bash -c "source /build/jenkins/workspace/setup.sh && cd $WORKSPACE && bash" 
EOF
chmod u+x rundocker.sh

# Run container and full build inside
docker run -e WORKSPACE=$WORKSPACE \
            -e LCG_VERSION=$LCG_VERSION \
            -e LCG_IGNORE="$LCG_IGNORE" \
            -e BUILDMODE=$BUILDMODE \
            -e LCG_INSTALL_PREFIX=$LCG_INSTALL_PREFIX \
            -e LCG_EXTRA_OPTIONS=$LCG_EXTRA_OPTIONS	  \
            -e TARGET=$TARGET		\
            -e TEST_LABELS=$TEST_LABELS \
            -e USER=sftnight			\
           	-e SHELL=$SHELL				\
            -e GIT_COMMIT=$GIT_COMMIT	\
            -e PROPERTIES_PATH=/lcgjenkins \
            -e RUN_TEST=$RUN_TEST \
            -e MODE=$MODE \
            -e LABEL=$LABEL \
            -e COPY_LOGS=$COPY_LOGS \
            -u sftnight		\
            --name $NAME				\
            --hostname $HOSTNAME-docker \
            --cpus=$DOCKER_CPUS			\
            -v /ccache:/ccache		\
            -v /ec/conf:/ec/conf 	\
            -v /cvmfs:/cvmfs      \
            -v $PWD/lcgjenkins:/lcgjenkins	\
            -v $PWD/lcgcmake:/lcgcmake 		\
            -v $PWD/lcgtest:/lcgtest 		\
            --mount type=bind,source="$WORKSPACE_HOST/docker",target=/build/jenkins/workspace \
            	  gitlab-registry.cern.ch/sft/docker:$DOCKER_IMAGE 	\
                bash -c "/lcgjenkins/$build_script $BUILDTYPE $COMPILER $LCG_VERSION"

# Info for triggered job
cp $WORKSPACE_HOST/lcgjenkins/properties.txt $WORKSPACE_HOST
