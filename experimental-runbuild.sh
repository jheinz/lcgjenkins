# setup environment -----------------------------------------------
echo source $WORKSPACE/lcgjenkins/jk-setup.sh $BUILDTYPE $COMPILER $SLOT > setup.sh
source lcgjenkins/jk-setup.sh $BUILDTYPE $COMPILER $SLOT
#---Create stampfile to enable our jenkins to purge old builds-----
touch $WORKDIR/controlfile
# clean up the WORKDIR --------------------------------------------
rm -rf $WORKDIR/build
rm -rf $WORKDIR/install
rm -rf /tmp/the.lock
# print environment -----------------------------------------------
env | sort | sed 's/:/:?     /g' | tr '?' '\n'
# do the build-----------------------------------------------------

ctest -VV -S lcgjenkins/lcgcmake-build.cmake 
EXITCODE=$?

cd $WORKSPACE
jsonfile="$SLOT-$PLATFORM.txt"
today=`date +%Y-%m-%d`
lcgjenkins/lcgsoft/ReleaseSummaryReader $SLOT $PLATFORM nightlies
lcgjenkins/lcgsoft/fill_release.py -f $WORKSPACE/$jsonfile -d "$today" -e "Daily publication of the $SLOT nightliy build" -o "NO"

# create view for the test
mkdir $WORKSPACE/view
$WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -l $WORKSPACE/install -p $PLATFORM -d -B $WORKSPACE/views

# collect env variables required for test
cat > $WORKSPACE/properties.txt << EOF
PLATFORM=${PLATFORM}
BUILDHOSTNAME=`hostname`
VIEW=$WORKSPACE/views
weekday=${weekday}
CTEST_TIMESTAMP=`head -1 $WORKSPACE/build/Testing/TAG`
CTEST_TAG=`tail -1 $WORKSPACE/build/Testing/TAG`
EOF

exit $EXITCODE
