# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Architecture'
        db.create_table(u'relinfo_architecture', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=60)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Architecture'], null=True, blank=True)),
        ))
        db.send_create_signal(u'relinfo', ['Architecture'])

        # Adding model 'Compiler'
        db.create_table(u'relinfo_compiler', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=60)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Compiler'], null=True, blank=True)),
        ))
        db.send_create_signal(u'relinfo', ['Compiler'])

        # Adding model 'Osystem'
        db.create_table(u'relinfo_osystem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=60)),
            ('parent', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Osystem'], null=True, blank=True)),
        ))
        db.send_create_signal(u'relinfo', ['Osystem'])

        # Adding model 'Mode'
        db.create_table(u'relinfo_mode', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=60)),
        ))
        db.send_create_signal(u'relinfo', ['Mode'])

        # Adding model 'Platform'
        db.create_table(u'relinfo_platform', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=135)),
            ('architecture', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Architecture'], null=True, blank=True)),
            ('osystem', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Osystem'], null=True, blank=True)),
            ('compiler', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Compiler'], null=True, blank=True)),
            ('mode', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Mode'], null=True, blank=True)),
            ('is_abstract', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'relinfo', ['Platform'])

        # Adding model 'License'
        db.create_table(u'relinfo_license', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=128)),
            ('url', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal(u'relinfo', ['License'])

        # Adding model 'Contact'
        db.create_table(u'relinfo_contact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=150, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(unique=True, max_length=75)),
        ))
        db.send_create_signal(u'relinfo', ['Contact'])

        # Adding model 'Package'
        db.create_table(u'relinfo_package', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(unique=True, max_length=135)),
            ('fullname', self.gf('django.db.models.fields.CharField')(max_length=135)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('category', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('language', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('license', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.License'], null=True, blank=True)),
            ('homepage', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal(u'relinfo', ['Package'])

        # Adding model 'PackageContact'
        db.create_table(u'relinfo_packagecontact', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('package', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Package'])),
            ('contact', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Contact'])),
        ))
        db.send_create_signal(u'relinfo', ['PackageContact'])

        # Adding unique constraint on 'PackageContact', fields ['package', 'contact']
        db.create_unique(u'relinfo_packagecontact', ['package_id', 'contact_id'])

        # Adding model 'Tag'
        db.create_table(u'relinfo_tag', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('package', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Package'])),
        ))
        db.send_create_signal(u'relinfo', ['Tag'])

        # Adding model 'TagDependency'
        db.create_table(u'relinfo_tagdependency', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('from_tag', self.gf('django.db.models.fields.related.ForeignKey')(related_name='from_tags', to=orm['relinfo.Tag'])),
            ('to_tag', self.gf('django.db.models.fields.related.ForeignKey')(related_name='to_tags', to=orm['relinfo.Tag'])),
            ('platform', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Platform'], null=True, blank=True)),
        ))
        db.send_create_signal(u'relinfo', ['TagDependency'])

        # Adding model 'Release'
        db.create_table(u'relinfo_release', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('version', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('description', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('type', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
            ('cancelled', self.gf('django.db.models.fields.DateTimeField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'relinfo', ['Release'])

        # Adding model 'ReleasePlatform'
        db.create_table(u'relinfo_releaseplatform', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('release', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Release'])),
            ('platform', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Platform'])),
        ))
        db.send_create_signal(u'relinfo', ['ReleasePlatform'])

        # Adding model 'ReleaseTag'
        db.create_table(u'relinfo_releasetag', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('release', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Release'])),
            ('tag', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Tag'])),
            ('platform', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Platform'], null=True, blank=True)),
        ))
        db.send_create_signal(u'relinfo', ['ReleaseTag'])

        # Adding model 'ReleasePackage'
        db.create_table(u'relinfo_releasepackage', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('release', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Release'])),
            ('package', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Package'])),
            ('platform', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['relinfo.Platform'], null=True, blank=True)),
        ))
        db.send_create_signal(u'relinfo', ['ReleasePackage'])


    def backwards(self, orm):
        # Removing unique constraint on 'PackageContact', fields ['package', 'contact']
        db.delete_unique(u'relinfo_packagecontact', ['package_id', 'contact_id'])

        # Deleting model 'Architecture'
        db.delete_table(u'relinfo_architecture')

        # Deleting model 'Compiler'
        db.delete_table(u'relinfo_compiler')

        # Deleting model 'Osystem'
        db.delete_table(u'relinfo_osystem')

        # Deleting model 'Mode'
        db.delete_table(u'relinfo_mode')

        # Deleting model 'Platform'
        db.delete_table(u'relinfo_platform')

        # Deleting model 'License'
        db.delete_table(u'relinfo_license')

        # Deleting model 'Contact'
        db.delete_table(u'relinfo_contact')

        # Deleting model 'Package'
        db.delete_table(u'relinfo_package')

        # Deleting model 'PackageContact'
        db.delete_table(u'relinfo_packagecontact')

        # Deleting model 'Tag'
        db.delete_table(u'relinfo_tag')

        # Deleting model 'TagDependency'
        db.delete_table(u'relinfo_tagdependency')

        # Deleting model 'Release'
        db.delete_table(u'relinfo_release')

        # Deleting model 'ReleasePlatform'
        db.delete_table(u'relinfo_releaseplatform')

        # Deleting model 'ReleaseTag'
        db.delete_table(u'relinfo_releasetag')

        # Deleting model 'ReleasePackage'
        db.delete_table(u'relinfo_releasepackage')


    models = {
        u'relinfo.architecture': {
            'Meta': {'object_name': 'Architecture'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Architecture']", 'null': 'True', 'blank': 'True'})
        },
        u'relinfo.compiler': {
            'Meta': {'object_name': 'Compiler'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Compiler']", 'null': 'True', 'blank': 'True'})
        },
        u'relinfo.contact': {
            'Meta': {'object_name': 'Contact'},
            'email': ('django.db.models.fields.EmailField', [], {'unique': 'True', 'max_length': '75'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '150', 'null': 'True', 'blank': 'True'})
        },
        u'relinfo.license': {
            'Meta': {'object_name': 'License'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '128'}),
            'url': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'})
        },
        u'relinfo.mode': {
            'Meta': {'object_name': 'Mode'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'})
        },
        u'relinfo.osystem': {
            'Meta': {'object_name': 'Osystem'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '60'}),
            'parent': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Osystem']", 'null': 'True', 'blank': 'True'})
        },
        u'relinfo.package': {
            'Meta': {'object_name': 'Package'},
            'category': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'contacts': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['relinfo.Contact']", 'through': u"orm['relinfo.PackageContact']", 'symmetrical': 'False'}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'fullname': ('django.db.models.fields.CharField', [], {'max_length': '135'}),
            'homepage': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'language': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'license': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.License']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '135'}),
            'type': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'relinfo.packagecontact': {
            'Meta': {'unique_together': "(('package', 'contact'),)", 'object_name': 'PackageContact'},
            'contact': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Contact']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Package']"})
        },
        u'relinfo.platform': {
            'Meta': {'object_name': 'Platform'},
            'architecture': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Architecture']", 'null': 'True', 'blank': 'True'}),
            'compiler': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Compiler']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_abstract': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'mode': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Mode']", 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '135'}),
            'osystem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Osystem']", 'null': 'True', 'blank': 'True'})
        },
        u'relinfo.release': {
            'Meta': {'object_name': 'Release'},
            'cancelled': ('django.db.models.fields.DateTimeField', [], {'null': 'True', 'blank': 'True'}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            'description': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'packages': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['relinfo.Package']", 'through': u"orm['relinfo.ReleasePackage']", 'symmetrical': 'False'}),
            'platforms': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['relinfo.Platform']", 'through': u"orm['relinfo.ReleasePlatform']", 'symmetrical': 'False'}),
            'tags': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['relinfo.Tag']", 'through': u"orm['relinfo.ReleaseTag']", 'symmetrical': 'False'}),
            'type': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'version': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'relinfo.releasepackage': {
            'Meta': {'object_name': 'ReleasePackage'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Package']"}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Platform']", 'null': 'True', 'blank': 'True'}),
            'release': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Release']"})
        },
        u'relinfo.releaseplatform': {
            'Meta': {'object_name': 'ReleasePlatform'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Platform']"}),
            'release': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Release']"})
        },
        u'relinfo.releasetag': {
            'Meta': {'object_name': 'ReleaseTag'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Platform']", 'null': 'True', 'blank': 'True'}),
            'release': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Release']"}),
            'tag': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Tag']"})
        },
        u'relinfo.tag': {
            'Meta': {'object_name': 'Tag'},
            'dependencies': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'users'", 'symmetrical': 'False', 'through': u"orm['relinfo.TagDependency']", 'to': u"orm['relinfo.Tag']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'package': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Package']"})
        },
        u'relinfo.tagdependency': {
            'Meta': {'object_name': 'TagDependency'},
            'from_tag': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'from_tags'", 'to': u"orm['relinfo.Tag']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'platform': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['relinfo.Platform']", 'null': 'True', 'blank': 'True'}),
            'to_tag': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'to_tags'", 'to': u"orm['relinfo.Tag']"})
        }
    }

    complete_apps = ['relinfo']