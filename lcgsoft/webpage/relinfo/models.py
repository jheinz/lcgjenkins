from django.db import models
import time
from functional import seq
from django.utils.functional import cached_property
import logging
from django.db import connection


class Architecture(models.Model):
    name = models.CharField(max_length=60, unique=True)
    parent = models.ForeignKey('self', null=True, blank=True)

    def __unicode__(self):
        return self.name


class Compiler(models.Model):
    name = models.CharField(max_length=60, unique=True)
    parent = models.ForeignKey('self', null=True, blank=True)

    def __unicode__(self):
        return self.name


class Osystem(models.Model):
    name = models.CharField(max_length=60, unique=True)
    parent = models.ForeignKey('self', null=True, blank=True)

    def __unicode__(self):
        return self.name


class Mode(models.Model):
    name = models.CharField(max_length=60, unique=True)

    def __unicode__(self):
        return self.name


class Platform(models.Model):
    name = models.CharField(max_length=135)
    architecture = models.ForeignKey(Architecture, null=True, blank=True)
    osystem = models.ForeignKey(Osystem, null=True, blank=True)
    compiler = models.ForeignKey(Compiler, null=True, blank=True)
    mode = models.ForeignKey(Mode, null=True, blank=True)
    is_abstract = models.BooleanField(default=False)

    def __unicode__(self):
        return self.name


class License(models.Model):
    name = models.CharField(max_length=128, unique=True)
    url = models.URLField(null=True, blank=True)

    def __unicode__(self):
        return self.name


class Contact(models.Model):
    name = models.CharField(max_length=150, null=True, blank=True)
    email = models.EmailField(unique=True)

    def __unicode__(self):
        return self.email


DATABASE_CAT = 0
GRAPHICS_CAT = 1
GRID_CAT = 2
IO_CAT = 3
MATH_CAT = 4
XML_CAT = 5
SIMULATION_CAT = 6
TOOL_CAT = 7
OTHER_CAT = 8
GENERATOR_CAT = 9

PACKAGE_CATEGORIES = (
    (DATABASE_CAT, u'Databases'),
    (GRAPHICS_CAT, u'Graphics'),
    (GRID_CAT, u'Grid'),
    (IO_CAT, u'IO'),
    (MATH_CAT, u'Math'),
    (XML_CAT, u'XML'),
    (SIMULATION_CAT, u'Simulation'),
    (TOOL_CAT, u'Tool'),
    (OTHER_CAT, u'Other'),
    (GENERATOR_CAT, u'Generator'),
)

C_LANG = 0
CPP_LANG = 1
FORTRAN_LANG = 2
JAVA_LANG = 3
PERL_LANG = 4
PYTHON_LANG = 5
OTHER_LANG = 6
FORTRAN = 2
PACKAGE_LANGUAGES = (
    (C_LANG, u'C'),
    (CPP_LANG, u'C++'),
    (FORTRAN_LANG, u'Fortran'),
    (FORTRAN, u'Fortran'),
    (JAVA_LANG, u'Java'),
    (PERL_LANG, u'Perl'),
    (PYTHON_LANG, u'Python'),
    (OTHER_LANG, u'Other'),
)

PROJECT_TYPE = 0
PACKAGE_TYPE = 1
METAPACKAGE_TYPE = 2
PACKAGE_TYPES = (
    (PROJECT_TYPE, u'Project'),
    (PACKAGE_TYPE, u'Package'),
    (METAPACKAGE_TYPE, u'Metapackage'),
)


class Package(models.Model):
    name = models.CharField(max_length=135, unique=True)
    fullname = models.CharField(max_length=135, unique=False)
    description = models.TextField(null=True, blank=True)
    type = models.IntegerField(choices=PACKAGE_TYPES, null=True, blank=True)
    category = models.IntegerField(
        choices=PACKAGE_CATEGORIES, null=True, blank=True)
    language = models.IntegerField(
        choices=PACKAGE_LANGUAGES, null=True, blank=True)
    license = models.ForeignKey(License, null=True, blank=True)
    homepage = models.URLField(null=True, blank=True)
    contacts = models.ManyToManyField(Contact, through='PackageContact')

    def __unicode__(self):
        return self.name

    @cached_property
    def getLastTag(self):
        return seq(self.tag_set.all())\
            .map(lambda tag: tag.name)\
            .sorted(reverse=True)\
            .head_option()


class PackageContact(models.Model):
    package = models.ForeignKey(Package)
    contact = models.ForeignKey(Contact)

    class Meta:
        unique_together = (("package", "contact"),)


class Tag(models.Model):
    name = models.CharField(max_length=100)
    package = models.ForeignKey(Package)
    dependencies = models.ManyToManyField(
        'self', symmetrical=False, through='TagDependency', related_name='users')

    def getLastRelease(self):
        """Return the last release of this tag"""
        return seq(self.releasetag_set.all())\
            .map(lambda rt: rt.release)\
            .sorted(key=lambda r: r.version, reverse=True)\
            .head_option()

    def __unicode__(self):
        return self.name

    def add_dependency_to(self, tag):
        dependency, created = TagDependencies.objects.get_or_create(
            from_tag=self, to_tag=tag)
        return dependency

    def remove_dependency_to(self, tag):
        TagDependencies.objects.filter(from_tag=self, to_tag=tag).delete()

    def get_dependencies(self):
        return self.dependencies.filter(to_tags__from_tag=self)

    def get_users(self):
        return self.users.filter(from_tags__to_tag=tag)

    def get_src_URL(self):
        if (self.package.category == GENERATOR_CAT):
            return "MCGenerators/distribution/%s/%s-%s-src.tgz" % (self.package.name, self.package.name, self.name)
        elif (self.package.type == PROJECT_TYPE):
            return "distribution/%s-%s-src.tgz" % (self.package.name, self.name)
        else:
            return "tarFiles/%s-%s.tgz" % (self.package.name, self.name)


class TagDependency(models.Model):
    from_tag = models.ForeignKey(Tag, related_name='from_tags')
    to_tag = models.ForeignKey(Tag, related_name='to_tags')
    platform = models.ForeignKey(Platform, null=True, blank=True)


LCG_RELEASE = 0
HEPSOFT_RELEASE = 1
RELEASE_TYPE = (
    (LCG_RELEASE, u'LCG Release'),
    (HEPSOFT_RELEASE, u'Hepsoft release'),
)


class Release(models.Model):
    version = models.CharField(max_length=30)
    description = models.TextField(null=True, blank=True)
    type = models.IntegerField(choices=RELEASE_TYPE, default=LCG_RELEASE)
    date = models.DateTimeField(auto_now_add=False)

    tags = models.ManyToManyField(Tag, through='ReleaseTag')
    platforms = models.ManyToManyField(Platform, through='ReleasePlatform')
    packages = models.ManyToManyField(Package, through='ReleasePackage')

    cancelled = models.DateTimeField(null=True, blank=True)

    release_notes = models.TextField(null=True, blank=True)
    extra_notes = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return self.version

    def getPackagesWithCategory(self, v):
        return Package.objects.filter(category=str(v))

    def getPackagesWithCategoryRelease(self, v):
        releases = map(lambda r: r.package_id,
                       ReleasePackage.objects.filter(release_id=self.id))
        return seq(self.getPackagesWithCategory(v))\
            .filter(lambda package: self.id in releases)


class ReleasePlatform(models.Model):
    release = models.ForeignKey(Release)
    platform = models.ForeignKey(Platform)


class ReleaseTag(models.Model):
    release = models.ForeignKey(Release)
    tag = models.ForeignKey(Tag)
    platform = models.ForeignKey(Platform, null=True, blank=True)


class ReleasePackage(models.Model):
    release = models.ForeignKey(Release)
    package = models.ForeignKey(Package)
    platform = models.ForeignKey(Platform, null=True, blank=True)
