if [ $# -eq 0 ]
then
  echo "You have to supply the admin password"
else
  export db_pass=${1}
  export WORKSPACE=$( cd "$( dirname "${BASH_SOURCE[0]}" )/../.." && pwd )
  export PYTHONPATH=$WORKSPACE/lcgjenkins/lcgsoft/webpage:$PYTHONPATH
fi
