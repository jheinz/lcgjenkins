import os
import argparse
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from relinfo.models import Package, PACKAGE_LANGUAGES, PACKAGE_TYPES, PACKAGE_CATEGORIES
from functional import seq
import json


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Script to simplify the modification of packages\' data')

    parser.add_argument('name', help='Package name')

    parser.add_argument('-p', '--pretty', help='Prints data in a pretty way', action='store_true')
    parser.add_argument('-d', '--description',
                        help='Set the package description')
    parser.add_argument('--home-page', help='Set the home page of the package')
    parser.add_argument('--full-name', help='Set the full name of the package')
    parser.add_argument('-c', '--category',
                        help='Set the category of the package')
    parser.add_argument('-t', '--type', help='Set the type of the package')
    parser.add_argument('-l', '--language',
                        help='Set the language of the package')

    parser.add_argument(
        '--delete', help="Deletes de selected package and all its data", action='store_true')

    args = parser.parse_args()

    try:
        package = Package.objects.get(name=args.name)

        package_dict = {
            'name': package.name,
            'description': package.description,
            'fullname': package.fullname,
            'homepage': package.homepage,
            'category': PACKAGE_CATEGORIES[package.category],
            'type': PACKAGE_TYPES[package.type],
            'language': PACKAGE_LANGUAGES[package.language]
        }
        if args.pretty:
            print json.dumps(package_dict, indent=4, sort_keys=True)
        else:
            print package_dict

        if args.description:
            package.description = args.description
            package.save()

        if args.full_name:
            package.fullname = args.full_name
            package.save()

        if args.home_page:
            package.homepage = args.home_page
            package.save()

        if args.category:
            cat = seq(PACKAGE_CATEGORIES)\
                .filter(lambda x: x[1] == args.category)\
                .map(x[0]).head_option()
            if cat:
                package.category = cat

        if args.type:
            type_value = seq(PACKAGE_TYPES)\
                .filter(lambda x: x[1] == args.type)\
                .map(x[0]).head_option()
            if cat:
                package.type = type_value

        if args.language:
            language = seq(PACKAGE_LANGUAGES)\
                .filter(lambda x: x[1] == args.language)\
                .map(x[0]).head_option()
            if cat:
                package.language = cat

        if args.delete:
            package.delete()
            print 'Deleted successfully'
    except Exception as e:
        print 'Package not found'
