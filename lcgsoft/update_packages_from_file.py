#!/usr/bin/env python

import argparse
import json
import os
os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
from relinfo.models import Package, PackageContact, Contact, License, PACKAGE_CATEGORIES, PACKAGE_LANGUAGES
from functional import seq


def check_package(package):
    package_db = Package.objects.get(name=package['name'])

    attrs = ['description', 'name', 'fullname', 'homepage']

    update = False
    for attr in attrs:
        if getattr(package_db, attr) != package[attr]:
            update = True
            setattr(package_db, attr, package[attr])

    if (package['language'] and not package_db.language) or \
            (package_db.language and PACKAGE_LANGUAGES[package_db.language][1] != package['language']):

        package_db.language = seq(PACKAGE_LANGUAGES).find(
            lambda l: l[1] == package['language'])
        update = True

    if (package['category'] and not package_db.category) or \
            (package_db.category and PACKAGE_CATEGORIES[package_db.category][1] != package['category']):

        package_db.category = seq(PACKAGE_LANGUAGES).find(
            lambda l: l[1] == package['category'])
        update = True

    if (package['license'] and not package_db.license) or \
            (package_db.license and package['license'] != package_db.license.name):

        package_db.license = License.objects.get_or_create(
            name=package['license'])
        update = True

    contacts = package_db.contacts.all()
    refreshContacts = len(package['contacts']) != package_db.contacts.count()
    if not refreshContacts:
        for contact in package['contacts']:
            if seq(contacts).filter(lambda c: c.name == contact['name'] and c.email == contact['email']).empty():
                refreshContacts = True
                update = True
                break
    if refreshContacts:
        package_db.contacts.clear()
        for contact in package['contacts']:
            contact_db = Contact.objects.get_or_create(
                name=contact['name'], email=contact['email'])
            pc_db = PackageContact(
                package_id=package_db.id, contact_id=contact_db[0].id)
            pc_db.save()

    if update:
        package_db.save()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Parses a json file containing information about the packages. If there is any difference with the DB, the DB is updated.')

    parser.add_argument(
        'path', help='Path of the JSON file with the package info')

    args = parser.parse_args()

    packages = json.load(open(args.path, 'r'))

    for package in packages:
        check_package(package)
