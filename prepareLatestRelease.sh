#!/bin/bash
if [ $# -ge 4 ]; then
  SLOT=$1; shift
  CORAL=$1; shift
  COOL=$1; shift
  ROOT=$1; shift
else
  echo "$0: expecting 4 arguments: [SLOT tag] [CORAL tag] [COOL tag] [ROOT tag]"
  return
fi

export SLOT
export CORAL
export COOL
export ROOT

dev4="$WORKSPACE/lcgcmake/cmake/toolchain/heptools-dev4.cmake"
latest="$WORKSPACE/lcgcmake/cmake/toolchain/heptools-latest.cmake"
tmpfile="$WORKSPACE/lcgcmake/cmake/toolchain/heptools-latest.cmake.tmp"

cp $dev4 $latest

sed -i '/CORAL/d' $latest
sed -i '/dev4/d' $latest
sed -i '/COOL/d' $latest
sed -i '/ROOT/d' $latest

sed "6i\set(heptools_version  $SLOT)" $latest > $tmpfile && mv $tmpfile $latest 
sed "10i\LCG_AA_project(CORAL  $CORAL)" $latest > $tmpfile && mv $tmpfile $latest 
sed "11i\LCG_AA_project(COOL  $COOL)" $latest > $tmpfile && mv $tmpfile $latest
sed "12i\LCG_AA_project(ROOT  $ROOT)" $latest > $tmpfile && mv $tmpfile $latest
