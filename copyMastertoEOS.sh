#!/bin/bash -x

# This script is intended to be executed in the Jenkins MASTER ONLY

export EOS_MGM_URL=root://eosuser.cern.ch

FILES=*.tgz
weekday=`date +%a`
txtfile=LCG_${LCG_VERSION}_${PLATFORM}.txt
isDone=isDone-${PLATFORM}
isDoneUnstable=isDone-unstable-${PLATFORM}
tarfiles=*-${PLATFORM}.tgz

if [[ ${PLATFORM} == *slc6* || ${PLATFORM} == *cc7* || ${PLATFORM} == *centos7* || ${PLATFORM} == *ubuntu* ]]; then
    kinit sftnight@CERN.CH -5 -V -k -t /ec/conf/sftnight.keytab
fi

if [ "${BUILDMODE}" == "nightly" ]; then
    basespace=/eos/project/l/lcg/www/lcgpackages/tarFiles/nightlies/$LCG_VERSION/$weekday
    localspace=/build/workspace/nightlies-tarfiles/$LCG_VERSION/$weekday
else
    basespace=/eos/project/l/lcg/www/lcgpackages/tarFiles/releases
    localspace=/build/workspace/releases-tarfiles
fi

rm $basespace/$txtfile
cd $localspace

if [ "${BUILDMODE}" == "nightly" ]; then 
    rm $basespace/$isDone
    rm $basespace/$isDoneUnstable
    rm $basespace/$tarfiles
    xrdcp -f $isDone root://eosuser.cern.ch/$basespace/$isDone 
    xrdcp -f $isDoneUnstable root://eosuser.cern.ch/$basespace/$isDoneUnstable
fi

xrdcp -f $txtfile root://eosuser.cern.ch/$basespace/$txtfile

find . -type f | sed -e 's/.*\.//' | sort | uniq -c | sort -n | grep -Ei '(tgz)$'
if [ $? != 0 ]; then
    echo "there are no .tgz files in this directory, we just go out"
    exit 0
    
else
    
    for files in $FILES
    do
        if [ "${BUILDMODE}" == "nightly" ]; then
	    xrdcp -f $files root://eosuser.cern.ch/$basespace/$files
            rm $files
        else

            xrdcp $files root://eosuser.cern.ch/$basespace/$files
	    rm $files
        fi
    done
    
    
fi
